import requests

sensor_data = { "temperature": 25.5,
                "humidity": 50.0, 
                "pressure": 1013.0 }

response = requests.post("http://localhost:8000/sensor_data", json=sensor_data)

print("Response status code:", response.status_code) 
print("Response content:", response.content)