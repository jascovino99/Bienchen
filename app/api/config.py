
import os
from dotenv import load_dotenv

class AppConfig:
    def __init__(self):
       
        load_dotenv()

        self.INFLUXDB_URL = os.getenv("INFLUXDB_URL", "http://localhost:8086")
        self.INFLUXDB_ORG = os.getenv("INFLUXDB_ORG", "Projekt Elektronik")
        self.INFLUXDB_BUCKET = os.getenv("INFLUXDB_BUCKET", "no")
        self.INFLUXDB_TOKEN = os.getenv("INFLUXDB_TOKEN", "djfMo1djgrTahXcFOfxe-XWGHcjrbiexVDuIRvLnjklbQBELc5yVIT2ylZcII7mXNNgSrIyI2D-VDDqP-fhgmQ==")
        self.API_HOST = os.getenv("API_HOST", "0.0.0.0")
        self.API_PORT = int(os.getenv("API_PORT", 8000))

