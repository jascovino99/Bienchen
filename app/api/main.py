import influxdb_client
from influxdb_client.client.write_api import SYNCHRONOUS
from influxdb_client import Point
from fastapi import FastAPI
from pydantic import BaseModel
import uvicorn
from config import AppConfig

config = AppConfig()

client = influxdb_client.InfluxDBClient(
    url=config.INFLUXDB_URL,
    token=config.INFLUXDB_TOKEN,
    org=config.INFLUXDB_ORG
)

app = FastAPI()

class SensorData(BaseModel):
    temperature: float
    humidity: float
    pressure: float

@app.post("/sensor_data")
async def receive_sensor_data(sensor_data: SensorData):
    print("Received sensor data:", sensor_data)

    point = Point("sensor_data") \
        .tag("location", "your_location") \
        .field("temperature",  sensor_data.temperature) \
        .field("humidity", sensor_data.humidity) \
        .field("pressure", sensor_data.pressure) 

    write_api.write(bucket=config.INFLUXDB_BUCKET, org=config.INFLUXDB_ORG, record=point)

write_api = client.write_api(write_options=SYNCHRONOUS)

if __name__ == '__main__':
    uvicorn.run(app, host=config.API_HOST, port=config.API_PORT)
